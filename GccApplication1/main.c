#include <avr/io.h>

void DisplayTime();
void DisplayFourUnderLineChar();

char IsStarButtonPressed();

void ClearDisplay();
void ShowModeOptions();
char GetMode();
char Get4DigitForTime();
char Get4DigitForAlarm();

char IsThereValidAlarmEvent();
void ArmTheAlarm();

char IsTimeSet;		//globalis valtozo, annak beallitasara, hogy van-e mar ervenyes ido beallitva
char SelectedMode;	// 1 vagy 2 lehet - 1: ora idobeallitasa, 2: ebreszto idobeallitasa

int main(void)
{

    while (1) 
    {
		if (IsTimeSet)
		{
			DisplayTime();					// ido kijelzese
		}
		else
		{
			DisplayFourUnderLineChar();		// ha nincs ervenyes ido, akkor 4 db alulvonas kijelzese
		}


		if(IsStarButtonPressed())
		{
			ClearDisplay();
			ShowModeOptions();
			char SelectedMode = GetMode();		// 1or2
			if (SelectedMode==1)
			{
				Get4DigitForTime();
			}
			else if (SelectedMode==2)
			{
				Get4DigitForAlarm();
			}
		}
		if(IsThereValidAlarmEvent())
		{
			ArmTheAlarm();
		}
	}
}

void DisplayTime()
{
	
}
void DisplayFourUnderLineChar()
{

}
char IsStarButtonPressed()
{

}
void ClearDisplay()
{

}
void ShowModeOptions()
{

}
char GetMode()
{

}
char Get4DigitForTime()
{

}
char Get4DigitForAlarm()
{

}
char IsThereValidAlarmEvent()
{

}
void ArmTheAlarm()
{

}
